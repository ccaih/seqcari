SEQCARI Decision Support Tool

Updated 29.01.2015

The tool no longer requires a nodejs based webserver to run as a separate service. 
The "downlaod all" functionality is now provided by a Python CGI script (seqcari.py).

The following entry in the apache config is required for this application to work (assuming the application is located at /opt/seqcari/):

ScriptAlias /seqcari/expresszip "/opt/seqcari/seqcari/seqcari.py"
Alias /seqcari /opt/seqcari/seqcari

<Directory /opt/seqcari/seqcari>
    Order allow,deny
    Allow from all
    Options +ExecCGI
    AddHandler cgi-script .cgi .py
</Directory>
