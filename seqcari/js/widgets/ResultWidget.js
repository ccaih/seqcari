(function ($) {

	AjaxSolr.ResultWidget = AjaxSolr.AbstractWidget.extend({
		afterRequest: function () {
			if (this.action == "count"){
				console.log("facetname");
				$("#returnCount").html(this.manager.response.response.docs.length + " matching documents <a href='#'' class='btn-small pull-right' id='clear'>Clear Results</a><a href='#'' class='btn-small pull-right' id='dlall'>Download all documents</a>");

			} else if (this.action == "start") {

				$("#choosefile").empty();

				for (var j = 0, k = this.manager.response.response.docs.length; j < k; j++) {
					var doc = this.manager.response.response.docs[j];
					$("#choosefile").append("<option></option>" + AjaxSolr.theme('snippet', doc));
				}
				$("#choosefile").chosen();

			} else if (this.action == "search"){

				$("#returnCount").html("<div class='loader'><img src='img/loading.gif' /></div>");
				for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
					var doc2 = this.manager.response.response.docs[i];
					$(this.target).append(AjaxSolr.theme('result', doc2, i));
				}
				$("#returnCount").html(this.manager.response.response.docs.length + " matching documents <a href='#'' class='btn-small pull-right' id='clear'>Clear Results</a><a href='#'' class='btn-small pull-right' id='dlall'>Download all documents</a>");
			}

			else {

				$("#returnCount").html("<div class='loader'><img src='img/loading.gif' /></div>");
				$(this.target).empty();

				var maxCount = 0;
				var objectedItems = [];
				for (var facet in this.manager.response.facet_counts.facet_fields[this.field]) {
					var count = parseInt(this.manager.response.facet_counts.facet_fields[this.field][facet]);
					if (count > maxCount) {
						maxCount = count;
					}
					objectedItems.push({ facet: facet, count: count });
				}
				//count of 'coastal management' = management count - planning count - emergency count
				objectedItems.map(function (item) {
								switch(item.facet){
									case "emergency":
									emergItem = item.count;
									break;
								
									case "planning":
									planItem  = item.count;
									break;

									case "management":
									manItem = item.count;
									break;
								}
						});
			
				var coastalNum = manItem - emergItem - planItem;
				var facet;
				var facetCount;
				var facetArray = [];
				var progsArray = [];
				
				for (var m = 0, n = objectedItems.length; m < n; m++) {
					facet = objectedItems[m].facet;
					facetCount = objectedItems[m].count;
					
					$("." + facet + " .facetcount").html(AjaxSolr.theme('facet', facet, facetCount));
					$(".coastal .facetcount").html(AjaxSolr.theme('facet', facet, coastalNum));

					//console.log(facet, facetCount);
				}
					  

					for (var i = 0, l = this.manager.response.response.docs.length; i < l; i++) {
						var doc2 = this.manager.response.response.docs[i];
						$(this.target).append(AjaxSolr.theme('result', doc2, i));
					}

					//get all the class documents
					$(".document").each(function(){
						progsArray.push($(this).html());
					});

					

					$.getJSON('data/descriptions.json', function(data) {
						var found = $.map(data, function(items) {
							$.each(progsArray, function(i,v){
								
						if (items.title.indexOf(v) >= 0) {
							//console.log(items.title, items.description);
							$(".document:contains('" + items.title + "')").parent().after("<br><span class=descriptive>" + items.description + "</span>");
							return items;
						}
						
					});
							
						});

					
					});
					//console.log(progsArray);

					$("#returnCount").html(this.manager.response.response.docs.length + " matching documents <a href='#'' class='btn-small pull-right' id='clear'>Clear Results</a><a href='#'' class='btn-small pull-right' id='dlall'>Download all documents</a>");
					


				}

			}

		});
})(jQuery);