jQuery(document).ready(function($) {

/*		To do:
		download all function

	
	Done:
    the .draggable li's can only be dropped on the well that has the same number as the parent div..
    add number as class when dragging..
    remove from the accordion list
    make note of the index of the li
    make the accordions droppable to their own class
	remove the html of the selected li and enter it where it is dropped
	disable dragging on droppped draggables
	return to original accordion when closed
	Post the built query to solr
	return solr document
	link to PDF
	clear the wells when search box is used, clear button is clicked
	add facet counts
			- need to query when item is returned to accordion to update the facet count - done
			- 'coastal' item is giving incorrect number or facets due it being a shared keyword
			- removed do to misleading <span class='facetcount'></span>
			*/


	var listCode;
	var dropClass;
	var draggableClass;
	var accParentID;
	var optionTexts = [];
	var is_last_item;
	var droppableHolderClass = "dropArea";
	var droppableArray = [];
	var draggableArray = [];
	var documentArray = [];
	var newquery = "";
	var Manager;
	var fileList = "";
	var only_one = new Object();
	var definitions = ["sector","theme","riskhazard","settlement"];
	var categories = ["themeTitles","sectorTitles","settlementTitles","riskTitles"];
	var sectorTitles = {"Coastal Management":"coastal", "Emergency Management":"emergency","Human Health":"health","Physical Infrastructure":"infrastructure","Urban Planning and Management":"planning","Whole of Human Settlement":"settlement"};
	var themeTitles = {"Leadership, including Community Leadership":"leadership", "Managing the (Urban) Environment":"environment", "Preparing the Community":"preparing", "Proactive (Anticipatory) Initiatives":"proactive", "Risk Communication":"communication","Support for Vulnerable Communities":"vulnerable", "Technological Development and Innovation":"technological", "Training and Education":"training"};
	var riskTitles = {"Bushfire":"bushfire", "Erosion":"erosion",  "Inundation":"inundation", "Heatwaves":"heatwaves", "Flooding":"flooding", "Sea level Rise":"level", "Storm Surge":"surge", "Storm Tides":"tides","Severe Storms":"severe"};
	var settlementTitles = {"Beach Front High-rise":"beach","Canal Estate":"canal","Master-planned Community":"master","Coastal middle suburb":"middle","Peri-urban Community":"peri","Regional Centre":"regional"};

	setDrags();
	init();

	//allow the tool button in the navbar to turn of other active buttons
	$("#tooltab a").click(function(){
		$("#abouttab li.active").removeClass("active");
	});

	$("#abouttab").click(function(){
		$("#tooltab li.active").removeClass("active");
	});

	function init() {
		//initiate the accordion
		$("#accordion").accordion();
		$(".accordion-inner ul li").addClass("draggable");
		//$("#theQuery").hide();
		$('#example').popover();
	}

	//lable the draggable list items from the data
	function setDrags(){
		$.each(categories, function (i,v) {
			$v = eval(v);
			//$v.sort();
			var dragHTML = "";
			$.each($v, function (n,na) {
				dragHTML += ("<li class='btn " + na + "'><span class='btnTitle'>" + n + "</span></li>");
			});

			$("ul#" + v).append(dragHTML);

		});
	}

	//add the chevron sign to accordion items
	$(".accordion-heading")
		.prepend("&nbsp;<i class='icon-play icon-grey'></i>&nbsp;")
		.on("click",function () {
			$(this).next(".collapse").collapse('toggle');
			$("i", this).toggleClass("icon-play icon-chevron-down");
			$(".in").collapse('toggle');
		})
		.on("hover",function(){
			$(this).css('cursor', 'pointer');
		});

	$(".accordion-body").on('hidden', function () {
		$(this).prev().find("i.icon-chevron-down").toggleClass("icon-play icon-chevron-down");
	});


	// click event

	$("body").on("click", ".draggable", function(){
		accRemovedFrom = $(this).parent().parent().parent().attr("id");
		accParentID = $(this).parent().parent().attr("id");
		$(this).addClass(accParentID);
		listCode = $(this);
		draggableClass = $(this).attr("class");
		$(listCode).detach();
		$(listCode).removeAttr("style");
		listHTML = $(listCode).html();
		$(listCode).html("");
		classes = draggableClass.split(' ');
		facetClass = classes[1];
		$(listCode).removeAttr("class").addClass("search-choice-item").addClass(accParentID).addClass(facetClass).append("<span>" + listHTML + "</span><a href='javascript:void(0)' class='search-choice-close'></a>");
		$(listCode).find("span.facetcount").detach();
		listCode.appendTo($("." + accParentID + " ul"));
		buildQuery();
		$(".search-choice-item .facetcount").html("");
		$("#search").find('input').val("");

		theSelectors(accParentID);

	});




	//close the choice and return to it's original accordion position
	$("body").on("click","a.search-choice-close", function() {
		myParent = $(this).parent();
		items = [];
		label = myParent.find("span").html();
		myParent.removeClass("search-choice-item");
		origClasses = myParent.attr("class").split(' ');
		origParentID = origClasses[0];
		origFacetClass = origClasses[1];
		myParent.remove();
		existingHTML = $("#" + origParentID + " ul").html();
		newlistCode = "<li class='btn " + origFacetClass + "'>" + label + "</li>";
		existingHTML += newlistCode;
		$("#" + origParentID + " ul").html(existingHTML);
		$("#" + origParentID + " ul li").each(function(){
			theclasses = $(this).attr("class").split(' ');
			theclassesfacet = theclasses[1];
			items.push("<li class='btn " + theclassesfacet + "'>" + $(this).html() + "</li>");
		});
		items.sort();
		$("#" + origParentID + " ul").html(items);
		//find if there are no more filled wells
		isanyleft();

		if (anyLeft === 0){
			doreset();
		} else {
			$("#theQuery").hide();
			init();
			buildQuery();
					}
	});

	function isanyleft(){
		anyLeft = $("." + droppableHolderClass + " ul li").length;
	}

	function theSelectors(theState,theWell){

		//have the booleans disabled when only one option
		//when more than one..strip out the html and reenter and add chosen

		var theParentDiv = $("." + theWell).parent();
		var optionSelected = theParentDiv.find("select option:selected").val();
		if(theState > 1){
			//console.log(only_one.well);
			theParentDiv.find(".booleanholder").html("Query: <select style='width:70px;' class='boolean'><option>AND</option><option>OR</option></select>");
			theParentDiv.find("select").val(optionSelected);
			theParentDiv.find(".boolean").chosen({ disable_search_threshold: 2 });
		} else {
			theParentDiv.find(".chzn-container").addClass("chzn-disabled");
			theParentDiv.find("select").prop("disabled", true);
		}
	}

	function doreset() {
		//clear the wells with class dropArea..remove all the html from the ul's..clear the results and returnCount
		$(".dropArea ul").empty();
		$("#results").empty();
		$("#returnCount").empty();
		$("#theQuery").hide();
		//reset the drag items to the original positions
		//clear the accordions
		$(".accordion-inner ul").empty();
		$(".facetcount").remove();
		setDrags();
		init();
	}

	$("body").on("click","#clear", function(){
		doreset();
	});

	$("body").on("click","#dlall", function(){
		downloadall();
	});

	function downloadall(){
		//add the file names as string to get request
		//get the href of all the class=document files and add to the string to send to express
		$("#downloader").remove();
		//expressURL = "http://localhost:3000/expresszip/?";
		expressURL = "/seqcari/expresszip/?";

		$(".document").each(function(){
			documentArray.push($(this).attr("href").substring(4));
		});

		$.each(documentArray, function(i,v){
			fileList += "name" + i + "=" + v + "";
			if (i != documentArray.length -1 ){
						fileList += "&";
					}
		});

		$("body").append("<iframe id=downloader src='" + expressURL + fileList + "' style='display: none;' ></iframe>");
		fileList = "";
		documentArray = [];
		//
	}


	/* Build the query
	*/

	//populate the dropdown of all titles at page load
	newquery = "*.*";
	action = "start";
	theManager(newquery,action);

	//choose indivdual files
	$("#choosefile").change(function () {
		window.open ($("#choosefile option:selected").val());
	});
	//set the ands or ors for each of the wells

	$(".boolean").chosen({ disable_search_threshold: 2 }).change(function(){
		buildQuery();
	});

	$("body").on("change",".boolean", function(){
		buildQuery();
	});

    //select 'and' or 'or' for query - AND as default
    var booleanDecider = "AND";
    $("#booleanDecision").chosen({ disable_search_threshold: 2 }).change(function () {
		booleanDecider =  $(this).val();
		buildQuery();
    });


	//search
	$("#search").find('button').bind('click', function(e) {
		doreset();
		newquery = $("#search").find('input').val();
		action = "search";
		$("#theQuery").show();
		theManager(newquery,action);
	});


    //build the query based on the selections
    function buildQuery (action) {
		$("#theQuery").show();
		booleanArray = [];
		newquery = "";

		//create array of the selectors
		$(".boolean").each(function(){
			booleanArray.push($(this).val());
		});


		$.each(definitions, function(i,v){
			window[v+'Array'] = [];
			$("." + v).each(function(){
				theArray = eval(v + "Array");
				$(this).find("li").each(function(w,e){

					//if there is only one..don't add the boolean seperator to the end
					theArray.push("\"" + $(e).find("span.btnTitle").html()  + "\"");
					only_one.state = theArray.length;
					only_one.well = v;
				});
			});

			$.each(theArray, function(m,k){
				if (theArray.length > 1){

					if (m != theArray.length - 1){
						theArray[m] = k + " " + booleanArray[i] + " " + "attr_subject:";
					}
				}
			});
			theSelectors(only_one.state,only_one.well);
		});

		sectorArray = $.map(sectorArray, function(val,index) {
			var str = val;
			return str;
		}).join("");

		themeArray = $.map(themeArray, function(val,index) {
			var str = val;
			return str;
		}).join("");
	
		riskhazardArray = $.map(riskhazardArray, function(val,index) {
			var str = val;
			return str;
		}).join("");
	
		settlementArray = $.map(settlementArray, function(val,index) {
			var str = val;
			return str;
		}).join("");
		
		bigArray = [];
		bigArray.push(sectorArray,themeArray,riskhazardArray,settlementArray);
		bigArray = $.grep(bigArray, function(n){ return (n); });

		$.each(bigArray, function(m,k){
			if (bigArray.length > 1){
				if (m != bigArray.length - 1){
					bigArray[m] = "(" + k + ") " + booleanDecider + " " + "attr_subject:";
				} else {
					bigArray[m] = "(" + k + ")";
				}
			}
		});

		bigArray = $.map(bigArray, function(val,index) {
			var str = val;
			return str;
		}).join(" ");

		//console.log(bigArray);
		newquery = bigArray;
		theManager(newquery,action);
	}



	//build the manager object from ajaxsolr

	function theManager(theQuery,theAction) {
		$(function () {

			Manager = new AjaxSolr.Manager({
				solrUrl: 'https://www.terranova.org.au/solr/'
				//solrUrl: 'http://localhost:8983/solr/'
			});

			Manager.addWidget(new AjaxSolr.ResultWidget({
				id: 'result',
				target: '#results',
				action : theAction,
				field : 'attr_subject'
			}));

			Manager.init();
			if (theAction == "search" || theAction == "start"){
				Manager.store.addByValue('q', '' + theQuery);
				theQuery = "";

			} else {

				var params = {
					facet: true,
					'q' : 'attr_subject:' + theQuery,
					'facet.field': [ 'attr_subject' ],
					'json.nl': 'map',
					'rows' : '95'
				};
				for (var name in params) {
					Manager.store.addByValue(name, params[name]);
				}
				//Manager.store.addByValue('q', 'attr_subject:' + theQuery);
				theQuery = "";
			}
			Manager.doRequest();

		});
	}
});