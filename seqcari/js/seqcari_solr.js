var Manager;
(function theManager($,theQuery) {
  $(function () {
    Manager = new AjaxSolr.Manager({
      solrUrl: 'http://localhost:8983/solr/'
    });
    Manager.addWidget(new AjaxSolr.ResultWidget({
      id: 'result',
      target: '#results'
    }));
    Manager.init();
    Manager.store.addByValue('q', 'attr_subject:coastal');
    Manager.doRequest();
    console.log(theQuery);
  });
})(jQuery);