(function ($) {

  AjaxSolr.theme.prototype.result = function (item, num) {

   var keywordslist = item.attr_subject.toString();
   keywordslist = keywordslist.replace(/\;/g, ', ');
   
   var output = '<tr class="resultitem"><td width="10">' + (num + 1) + '</td><td><strong><a target="_blank" class="document" href="pdf/' + item.attr_stream_name + '">' + item.attr_dc_title + '</a></strong><br><span ><small>Keywords: ' + keywordslist + '</small></span></td></tr>';
   
   return output;
 };

 AjaxSolr.theme.prototype.facet = function (facetname, count) {
   var output = count;
   return output;
 };

 AjaxSolr.theme.prototype.snippet = function (doc) {
   var output = '<option value="pdf/' + doc.attr_stream_name + '">' + doc.attr_dc_title + '</option>';
   return output;
 };

})(jQuery);
