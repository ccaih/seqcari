#!/usr/local/bin/python2.7
import cgi
import cgitb
from zipfile import ZipFile
from StringIO import StringIO
from os.path import split, join, realpath, exists
from shutil import copyfileobj
import sys 

# https://130.102.154.75/seqcari/expresszip/?name0=CM%201%20CM%201.3.pdf&name1=CM%201%20CM%201.1.pdf&name2=CM%201%20CM%201.2.pdf&name3=CM%202%20CM%202.1.pdf&name4=CM%203%20CM%203.1.pdf&name5=CM%203%20CM%203.2.pdf&name6=CM%204%20CM%204.1.pdf&name7=CM%204%20CM%204.2.pdf&name8=CM%205%20CM%205.1.pdf&name9=CM%205%20CM%205.2.pdf&name10=CM%205%20CM%205.3.pdf

pdfpath = join(split(realpath(__file__))[0],"pdf")

cgitb.enable()


def main():
    form = cgi.FieldStorage()
    filenames = map(lambda y: form[y].value, filter(lambda x: x.startswith("name"), form.keys()))
    filenames = map(lambda x: split(x)[-1], filenames)
    filepaths = map(lambda fn: join( pdfpath, fn ), filenames )
    if len(filenames) == 0:
        print "Status:400"
        print "Content-Type: text/html\n"
        print "<h1>400 Bad Request</h1>\nNo documents selected."
    elif all( map(lambda x: exists(x), filepaths) ):
        print "Content-type: application/zip"
        print "Content-Disposition: attachment; filename=SEQCARI_documents.zip\n" 
        sys.stdout.flush()
        zip_flo = StringIO()
        zf = ZipFile(zip_flo, 'w')
        for fn, fp in zip(filenames, filepaths):
            zf.write(fp, fn)
        zf.close()
        zip_flo.seek(0)
        copyfileobj(zip_flo, sys.stdout)
    else:
        print "Status:404"
        print "Content-Type: text/html\n"
        print "<h1>404 Not Found</h1>\nOne or more of the documents do not exist.<br>If you believe this is incorrect, please contact the Terranova site administrator."


if __name__ == "__main__":
    main()
