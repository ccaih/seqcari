Sector
Theme
Risk
Settlement type


Program name


<title>Program Evaluation </title>

<table>
<th>What if questions (Assess on a threefold scale: low; medium; and high) </th>
	  - What is the likelihood that it will have negative impacts on society?
	  - To what extent does it represent “value for money” (cost effectiveness)?
	  - What is its likelihood of success (achieving aims)?
	  - To what degree does it promote rather than inhibit resilience (see definition)?
	   (definitions)
</table>

<table>
 <th>How well Program will address climate change (Appraisal Criteria: “high” = favourable; “low” = unfavourable) – refer “Climate change adaptation options appraisal criteria”</th>
	  - Flexibility
	  - Robustness
	  - Equity
	  - Coherence/Alignment
	  - Acceptability - political, community, bureaucracy,private sector
	  - Avoidance of maladaptation – Low GHG emissions
	  - Avoidance of maladaptation – Less Vulnerable populations
	  - Avoidance of maladaptation – Low Opportunity Costs
	  - Avoidance of maladaptation – Adaptation Incentives
	  - Avoidance of maladaptation – Low Path Dependency
</table>

<table>
<th>Outline of Program </th>
	 - Implementation Mechanisms
	 - Primary Responsibility
	 - Area or Location
	 - Type of Implementation Response
	 - Timing of Implementation
	 - Program Linkages
	 - Source/s (evidence)
</table>

<table>
Actions (X)
</table>

<table>
<th>Actions Implementation</th>
	 - Action
	 - Implementation Mechanisms
	 - Primary Responsibility
	 - Type of Implementation Response
	 - Program Linkages
	 - Source/s (evidence)
</table>

<table>
<th>Action Monitoring and Evaluation </th>
	 - Action
	 - Key performance indicators
	 - Opportunities for monitoring and evaluation (e.g. existing review cycles)
	 - Primary Responsibility (plus associated responsibility/ies)
</table>